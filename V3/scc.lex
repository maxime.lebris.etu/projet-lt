%{
/* Fichier scc.lex */
#include "yystype.h"
#include "yygrammar.h"

char err[20]; /* Chaine de caracteres pour les erreurs de syntaxe */
%}

/* Definition de macros */
separateur      [ \t]
lettre          [a-zA-Z]
chiffre         [0-9]
tiret           [-_]

%%

"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"  return PROLOGUE;            			/* Indique au parser que <?xml version="1.0" encoding="UTF-8"?> est reconnu */
"<scxml"               			      return SCXMLDEBUT;            			/* Indique au parser que <scxml est reconnu */
"xmlns"                                       return XMLNS;              			/* Indique au parser que xmlns est reconnu */
"="                                           return EQUAL;              			/* Indique au parser que le caractere = est reconnu */
">"                                           return CHEVRONFIN;				/* Indique au parser que le caractere > est reconnu */
"/>"                                          return BALISEFIN;					/* Indique au parser que la chaine /> est reconnue */
"\"http://www.w3.org/2005/07/scxml\""         return REF;              				/* Indique au parser que la reference "http://www.w3.org/2005/07/scxml" est reconnue */
"initial"                                     return INIT;              			/* Indique au parser que la chaine initial est reconnue */
"</scxml>"                		      return SCXMLFIN;              			/* Indique au parser que la balise de fin </scxml> est reconnue */
"<state"               			      return ETATDEBUT;           		        /* Indique au parser que <state est reconnu */
"</state>"               		      return ETATFIN;              			/* Indique au parser que la balise de fin </state> est reconnue */
"id"               			      return ID;              				/* Indique au parser que id est reconnu */
"final"               			      return FINAL;              			/* Indique au parser que final est reconnu */
"\"true\""             		              return CHTRUE;              			/* Indique au parser que "true" est reconnu */
"\"false\""             		      return CHFALSE;              			/* Indique au parser que "false" est reconnu */
"<transition"               		      return TRANSDEBUT;              		        /* Indique au parser que <transition est reconnu */
"event"                                       return EVENT;                                     /* Indique au parser que event est reconnu */
"target"              			      return TARGET;                                    /* Indique au parser que target est reconnu */
\"{lettre}({lettre}|{chiffre}|{tiret})*\"     { yylval.valChaine = (char*)malloc((strlen(yytext)+1)*sizeof(char));
		  				strcpy (yylval.valChaine, yytext);
		  				return CHAINE; }              			/* Indique au parser que le nom de l etat est reconnu */
{separateur}+    			      ;                        			        /* Elimination des espaces */
\n                			      yypos++;                  			/* Compte le nombre de lignes du fichier source */
.                                             { sprintf(err,"Mauvais caractere %c",yytext[0]);
                                              	yyerror(err);           			/* Generation d'une erreur de syntaxe */
                  	                      }

%%
